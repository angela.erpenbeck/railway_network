# Copyright <2022> <Angela Erpenbeck, Ella Fink, Wiebke Janßen >

import sys
import os, os.path
import process_data 
import downloading_feature
import search_city
from timetable import timetable
from PySide6 import QtCore
from PySide6 import QtWidgets
from PySide6 import QtGui
from PySide6.QtWidgets import (QPushButton, QMessageBox)
from PySide6.QtGui import QAction
from PySide6.QtCore import (Qt, QTimer)

class MapView(QtWidgets.QGraphicsView):
    """ Class to create the properties of the Map:
            - zoom Mode
            - drag Mode
            - show city name
            
        returns:
            current_city (str) =  saves the current city value as a string
            cityClicked (str)  =  saves the Signal of the city that is 
                                clicked as a string
    """

    # Sends the signal of the current choosen city and the clicked city 
    current_city = QtCore.Signal(str)
    cityClicked = QtCore.Signal(str)
    
    def __init__(self, parent):
        """ Constructor of the MapView class
        Initialises settings of the zoom, the drag mode, the mouse tracking 
        and the minimum Size of the widget
        
        returns:
            _zoom (variable) = set the zoom on zero (integer) to set the base 
            previous_item (variable) = set the previous_item to None,
                                        become a string
        """
        
        super(MapView, self).__init__(parent)
        self._zoom = 0
        # settings for the Viewer: 
        # set the Anchor unter the Mouse 
        self.setTransformationAnchor\
            (QtWidgets.QGraphicsView.AnchorUnderMouse) 
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        # set the Scrollbars off 
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        # set the Drag mode on and set a hand icon for the mouse 
        self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)
        self.setFrameShape(QtWidgets.QFrame.NoFrame)  
        # set the minimum size of the window 
        self.setMinimumSize(512, 512)
        # set mouse tracking true
        self.setMouseTracking(True)
        self.previous_item = None
    
    def mousePressEvent(self, event):
        """ Initialises the press event of the mouse
        
        Args:
            event (parameter) =  events occur when mouse button is pressed
        returns:
            item.city (variable)= the name of the city that is send to the 
                                signal 
            item (variable) = returns the position of the 
                             QGraphicpolygonItem or QGraphicEllipseItem 
                             that is pressed with two parameters
        """

        # if query to save the position of the mouse in the
        # variable 'item'. If Item is city, it sends the value item.city
        # if not it sends to the view no city is pressed 
        super(MapView, self).mousePressEvent(event)
        item = self.itemAt(event.position().toPoint())
        
        if item is not None and hasattr(item, 'city'): 
            self.cityClicked.emit(item.city)
        else:
            self.cityClicked.emit("No city is selected")          
 
    def mouseMoveEvent(self, event):
        """ Initialises the move event of the mouse 

        Args:
            event (parameter) =  events occur when mouse cursor is moved
        returns:
            item.city (str) = name of the current City as string
            
        """

        # if query to save the position of the movement of the mouse for 
        # thestatusbar in class MainWindow 
        super(MapView, self).mouseMoveEvent(event)
        item = self.itemAt(event.position().toPoint())
                    
        if self.previous_item is not None:
            self.previous_item.setPen(self.city_pen)
            self.previous_item = None
            self.current_city.emit(None)
        if item is not None and hasattr(item, 'city'):            
            item.setPen(self.mark_city_pen)
            item.setBrush(self.mark_city_brush)
            self.previous_item = item
            self.current_city.emit(item.city)
    
 
    def fitInView(self, scale=True):
        """ Initialises the zooming factor and fits the scene in view 
        
        Args:
            scale (bool) = activate the current view transformation
        source:
            https://stackoverflow.com/questions/35508711/how-to-enable-pan-and-zoom-in-a-qgraphicsview
        """

        # set the scene as rectangle, transform the scene to the rigt turn
        # scales the scene with the faktors from the wheelEvent(),
        # set the _zoom to zero 
        rect = self.sceneRect()
        unity = self.transform().mapRect(QtCore.QRectF(0, 0, 1, -1))
        self.scale(1 / unity.width(), 1 / unity.height())
        viewrect = self.viewport().rect()
        scenerect = self.transform().mapRect(rect)
        factor = min(viewrect.width() / scenerect.width(),
                        viewrect.height() / scenerect.height())
        self.scale(factor, factor)
        self._zoom = 0
        
    def wheelEvent(self, event):
        """ Initialises the wheelEvent for zooming 
        
        Args:
            event (parameter) =  events occur when mouse wheel is moved
        returns:
            faktor (variable) = returns faktor of the zoom as an integer 
        source:
            https://stackoverflow.com/questions/35508711/how-to-enable-pan-and-zoom-in-a-qgraphicsview
        """

        # the angleDelta returns delta of the angle of the mouse wheel
        # if angleDelta is positive the zoom is increased by one
        # if angleDelta is negative the zoom is decreased by one
        if event.angleDelta().y() > 0:
            factor = 1.25
            self._zoom += 1
        else:
            factor = 0.8
            self._zoom -= 1

        # adjust the scaling of the widget depending on self._zoom
        if self._zoom > 0:
            self.scale(factor, factor)
        elif self._zoom == 0:
            self.fitInView(self.sceneRect())
        else:
            self._zoom = 0      

    def toggleDragMode(self):
        """ Initialises the drag mode during zooming 
        source:
            https://stackoverflow.com/questions/35508711/how-to-enable-pan-and-zoom-in-a-qgraphicsview
        """
        # if query to see if drag mode is used or not 
        if self.dragMode() == QtWidgets.QGraphicsView.ScrollHandDrag:
            self.setDragMode(QtWidgets.QGraphicsView.NoDrag)
        elif not self._photo.pixmap().isNull():
            self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)

class TrainTimesModell(QtCore.QAbstractTableModel):
    """ Class to create a table with infomation about:
            - arrival
            - departure
            - destination
            - city name
            - trip id
    """

    def __init__(self):
        super().__init__()
        """ Constructor of the TrainTimeModell class

        Init to read the merged csv data 
        """
        
        str = "No city is selected"
        self.dataframe = timetable(str)
        self.display_city("No city is selected")

    @QtCore.Slot(str)
    def display_city(self, city):
        """ Recieves signal (item) of cityClicked,
            finds same item in dataframe.
        
            returns:
                current_data = receives item of cityClicked 
                and finds corresponding item in dataframe.
        """

        self.current_data = self.dataframe[self.dataframe.stop_name==city]
        self.layoutChanged.emit()

    def rowCount(self, parent=None):
        """ Counts rows present in dataframe.
        
        returns:
            len(self.current_data) = length of the dataframe
        """

        return len(self.current_data)

    def columnCount(self, parent=None):
        """ Counts columns present in dataframe.
        
        returns:
            len(self.current_data.columns) = number of columns in dataframe
        """

        return len(self.current_data.columns)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """ Displays the given data from dataframe """

        if role != QtCore.Qt.DisplayRole:
            return None

        stop = self.current_data.iloc[index.row(), index.column()]
        return str(stop)

    def headerData(self, index, orientation, 
                    role=QtCore.Qt.DisplayRole):
        """ Displays the header data of the given dataframe """

        if role != QtCore.Qt.DisplayRole or orientation != \
            QtCore.Qt.Orientation.Horizontal: 
            return None

        return self.current_data.columns[index]

class MainWindow(QtWidgets.QMainWindow):
    """ Class to create the main window with:
            - status bar
            - scene with map of class MapView()
            - widget with table of class TrainTimesModell() 
            - menu bar
    """

    def __init__(self):
        """ Constructor of the MapWindow class
        Initialises the statusbar, calls the class MapView and 
        TrainTimesModell and set brushes and pens for drawings in map
        returns: 
            self.dataframe (pandas DataFrame) = see module process_data
            self.trip_id_dict (dictionary)= see module process_data
            self.stop_name_dict (dictionary)= see module process_data
            self.status_bar (QStatusBar) = contains the status bar
            self.background_brush (QBrush) = sets the coulor for the 
                                            background
            self.country_brush (QBrush) = set the brush for the countries
            self.city_brush (QBrush) =  set the brush for the citys
            self.country_pen (QPen) = sets the pen for the country outline
            self.city_pen (QPen) = sets the pen for the cities
            self.mark_city_pen (QPen) = sets the pen for the city if it is  
                                        pressed
            self.mark_city_brush(QBrush) = sets the brush for the city if   
                                           it ispressed
            self.line_pen (QPen) = sets the pen for the lines of the routs
            self.map_view (MapView Class)= initialises the new class 
                                           Mapview from QGraphicView
            self.map_view.city_pen = sets the city pen for the map_view 
            self.map_view.city_brush = sets the city brush for the map_view 
            self.map_view.mark_city_pen = sets the marked city pen 
                                        for the map_view 
            self.map_view.mark_city_brush = sets the marked city brush
                                        for the map_view 
            table_view (QTableView)= initialises the new table view object
            table_model(QAbstractTableModel)= initialises the new class 
                                 TrainTimeModell from QAbstractTableModel
            city_label (QLable) = sets the Ciy label  
            self.line_list (variable, list)= initialises the line list 
                                            and sets it on None
            button_download (QButton)= creats a download button
            button_readme (QButton)= creats a download button
            button_license (QButton)= creats a download button
            window_content(QWidget) = creats a the content of the MainWindow
                                      in a widget 
        """
        
        super().__init__()
        # initialize the modul process_data to merge the files and returns them in the 
        # in self variable dataframe, trip id dictionary and stop name dictionary 
        self.dataframe, self.trip_id_dict, self.stop_name_dict = \
                    process_data.data_merge()
                
        # create status bar
        self.status_bar = self.statusBar()
        
        # sets color and width for the used pens and brushes for drawing
        # the city points, the lines, the countrys and the background
        self.background_brush = QtGui.QBrush("lightgreen", 
                                        QtCore.Qt.BrushStyle.SolidPattern)        
        self.country_brush = QtGui.QBrush("white", 
                                        QtCore.Qt.BrushStyle.SolidPattern)
        self.country_pen = QtGui.QPen("black")
        self.country_pen.setWidthF(0.04)
        self.city_pen = QtGui.QPen("blue")   
        self.city_pen.setWidthF(0.04)
        self.city_brush = QtGui.QBrush("blue", 
                                        QtCore.Qt.BrushStyle.SolidPattern)
        self.mark_city_pen = QtGui.QPen("red")   
        self.mark_city_pen.setWidthF(0.2)
        self.mark_city_brush = QtGui.QBrush("red", 
                                        QtCore.Qt.BrushStyle.SolidPattern)
        self.line_pen = QtGui.QPen("red")
        self.line_pen.setWidthF(0.03)
        
        # initialize the Class MapView in mapview 
        self.map_view = MapView(self)
        self.map_view.setScene(self.create_scene())
        self.map_view.setRenderHint(QtGui.QPainter.Antialiasing)
        
        # connect the map status bar with the current city variable to 
        # show the current city 
        self.map_view.current_city.connect(self.status_bar.showMessage)
        
        # profits the pens and brushes for the map_view
        self.map_view.city_pen = self.city_pen
        self.map_view.city_brush = self.city_brush
        self.map_view.mark_city_pen = self.mark_city_pen
        self.map_view.mark_city_brush = self.mark_city_brush

        # initialize the table view starts the TrainTimeModell Class to 
        # form the table
        table_view = QtWidgets.QTableView()
        table_model = TrainTimesModell()
        table_view.setModel(table_model)
        # hides the not used columns       
        table_view.hideColumn(4)
        table_view.hideColumn(5)
        table_view.hideColumn(7)    
          
        # connects the funktion display city from the table model with the 
        # map view class  to show the displayed city name 
        self.map_view.cityClicked.connect(table_model.display_city)
        # starts the create routes function to draw the fitting routes in 
        # the map 
        self.map_view.cityClicked.connect(self.create_routes)

        # set the city label for the header over the table 
        city_label = QtWidgets.QLabel("No city is selected")
        city_label.setAlignment(QtCore.Qt.AlignCenter)
        # connects the viewer with the city_label text 
        self.map_view.cityClicked.connect(city_label.setText)
        
        # set the line list on None 
        self.line_list = None 

        # initialises download button of menue bar 
        button_download = QAction("Download", self)
        button_download.setStatusTip("Download of the newest map")
        button_download.triggered.connect(self.download_button_click)
        
        # initialises update button of menue bar
        button_update = QAction("Update", self)
        button_update.triggered.connect(self.update_world)
        button_update.setStatusTip("Updates the map with the\
            newest downloaded map data")

        # initialises update button of menue bar
        button_search = QAction("Search", self)
        button_search.triggered.connect(self.search_button_click)
        button_search.setStatusTip("Search a city of the map")

        # initialises readme button of menue bar
        button_readme = QAction("Readme", self)
        button_readme.setStatusTip("Shows the ReadMe for more informations") #
        button_readme.triggered.connect(self.readme_button_click)
        
        # initialises license button of menue bar
        button_license = QAction("License", self)
        button_license.setStatusTip("Shows the license of the code")
        button_license.triggered.connect(self.license_button_click)
        
        # choose_network_layout = QtWidgets.QHBoxLayout()

        button_long_distance = QPushButton("Fernverkehr")
        button_long_distance.clicked.connect(self.choose_network)
        button_regional = QPushButton("Regionalverkehr")
        button_regional.clicked.connect(self.choose_network)
        button_local_transport = QPushButton("Nahverkehr")
        button_local_transport.clicked.connect(self.choose_network)

        # create menue bar with menu points 'Menu', 'Search' and 'Help'
        menu = self.menuBar()
        file_menu = menu.addMenu("&Menu")
        file_menu.addAction(button_download)
        file_menu.addAction(button_update)
        menu.addSeparator()
        file_search = menu.addMenu("&Search")
        file_search.addAction(button_search)
        menu.addSeparator()
        file_help = menu.addMenu("&Help")
        file_help.addAction(button_readme)
        file_help.addAction(button_license)
        
        # initialises layouts for the integration of the table and the
        # the associated city name
        button_layout = QtWidgets.QHBoxLayout()
        button_layout.addWidget(button_long_distance)
        button_layout.addWidget(button_regional)
        button_layout.addWidget(button_local_transport)
        vlayout = QtWidgets.QVBoxLayout()
        vlayout.addWidget(city_label)
        vlayout.addWidget(table_view)
        layout = QtWidgets.QGridLayout()
        layout.addWidget(self.map_view, 1, 0)
        layout.addLayout(vlayout, 1, 1)
        layout.addLayout(button_layout, 0, 0)
        
        # set layout of the central widget 
        window_content = QtWidgets.QWidget()
        window_content.setLayout(layout)
        self.setWindowTitle("railway_network")
        self.setCentralWidget(window_content)
        
        # single-shot timer fires only once, to create a fitting view in 
        # the startof the programm
        QTimer.singleShot(0, self.handle_timeout)
    
    def handle_timeout(self):
        """ creates a fitting viewscaled as large as possible inside a 
        given rectangle for the start of the programm, 
        preserving the aspect ratio
        """

        self.map_view.fitInView(Qt.KeepAspectRatio)
        
    def update_world(self):
        """ updates the created scene after downloading new map data """

        # set the new scene by starting the create_scene function
        self.map_view.setScene(self.create_scene())

        # if the line list is not clear, emptys the line list for the new 
        # create routes function (error prevention)
        if self.line_list != None:
            self.line_list.clear()
    
    def create_scene(self):
        """ Initialises the scene by using the polygone of
        map data and drawing ellipse at city coordinates

        return: 
            scene (QGraphicsScene) = consists of the polygone of
                               the countries of europe with 
                               are brushed in white, a background 
                               which is green and the ellipse 
                               on the city coordinates
        """

        # creates a scene object with the functions of QGraphicsScene
        scene = QtWidgets.QGraphicsScene()
        scene.setBackgroundBrush(self.background_brush)
        
        # loads the map_data of some europe countries 
        map_data = process_data.load_map_data()

        # for loop to draw the polygone of the countries
        # with the function addPolygon from QGraphicsScene
        for polygons in map_data:
            for polygon in polygons:
                qpolygon = QtGui.QPolygonF() 
                for x, y in polygon:
                    qpolygon.append(QtCore.QPointF(x, y))
                scene.addPolygon(qpolygon, 
                                pen=self.country_pen, 
                                brush=self.country_brush)           
                
        # creates a list for the stop ids of the citys 
        stop_id_list =[]

        # for- loop to draw the ellipse of the city just once  
        for idx, row in self.dataframe.iterrows():
            if row.stop_id in stop_id_list:
                continue
            ellipse = scene.addEllipse(row.stop_lon, 
                            row.stop_lat, 
                            0.04,0.04,
                            pen=self.city_pen,
                            brush=self.city_brush)
            ellipse.city = row.stop_name
            stop_id_list.append(row.stop_id)

        return scene
    
    @QtCore.Slot(str)
    def create_routes(self, city):
        """ creates the painted route depending on the route id 
        Args:
            city (variable) = string with the name of the clicked city 
        returns:
            self.line_list (variable) = list of the lines that are printed on 
                            the scene after one city is clicked
        """

        # if query to empty the line list
        if self.line_list != None:
            for line in self.line_list:
                self.map_view.scene().removeItem(line)
            self.line_list.clear() 
           
        # if query to stop the function if no city is pressed, or a empty
        # sting or the string "No city is selected"
        if city == None or city == "" or city =="No city is selected":
            return       
        
        # saves the trip ids of the stop names with the keys of the city 
        # from stop_name_dict in trip_id_list
        trip_id_list = self.stop_name_dict[city]
        
        #initialises the list line_list(list) and first_row variable(None)
        line_list = []
        first_row = None

        # for- loop to draw the lines of the trips of the clicked city
        # dependig on the rows of the 'trip_id_dict' dictionary with 
        # addLine function from QGraphicScene also depending on the 
        # stop_sequence collumn to draw from the beginning of the 
        # sorted trip id list with the stop names  
        for trip_id in trip_id_list:
            row_list = self.trip_id_dict[trip_id]
            for row in row_list:
                if row.stop_sequence == 0:
                    first_row = row
                    continue
                line = self.map_view.scene().addLine(first_row.stop_lon, 
                                    first_row.stop_lat,
                                    row.stop_lon, 
                                    row.stop_lat, 
                                    pen=self.line_pen)
                line_list.append(line)
                first_row = row
        self.line_list = line_list

    def download_button_click(self):
        """ try to create a folder 'Map_Data' 
        
        source: https://www.delftstack.com/de/howto/python/how-to-delete-a-file-and-directory/#l%25C3%25B6schen-einer-datei-in-python
        """

        # sets the path and try to create the folder if it is not existing
        path = r'Map_Data'
        try: 
            os.mkdir(path)
        except FileExistsError:
            pass  

        # calls the class DownloadingWindow to start the download after
        # pushing the download button
     
        download.show()

    def readme_button_click(self):
        """  Reads the file 'Readme.txt' and creates a QMessageBox 
        in which the text from 'Readme.txt' is displayed
        """

        # reads the file
        with open('ReadMe.txt', 'r', encoding='utf8') as readme:
            data_readme = readme.read()

        # creates the QMessageBox with title and text
        readme_window = QMessageBox(self)
        readme_window.setWindowTitle("ReadMe")
        readme_window.setText(data_readme)
        readme_window.exec()

    def license_button_click(self):
        """  Reads the file 'License_BSD.txt' and creates a QMessageBox 
        in which the text from 'License_BSD.txt' is displayed
        """

        # reads the file
        with open('License_BSD.txt', 'r', encoding='utf8') as license:
            data_license = license.read()
        
        # creates the QMessageBox with title, infomation icon and text
        license_window = QMessageBox(self)
        license_window.setIcon(QMessageBox.Information)
        license_window.setWindowTitle("License")
        license_window.setText(data_license)
        license_window.exec()

    def search_button_click(self):
        """  Open the search window after prushing the button of 
        'Search'
        """
        search_city.SearchWindow(MainWindow)
        search.show()
    
    @QtCore.Slot(str)
    def choose_network(self):
        """ Choose data for table to display for distance transport.
        
        returns:
            Signal for TrainTimesModell to choose right data.
        """
        #code is not completed because we ran out of time        
        return None

if __name__ == "__main__":
    """  Initialized MainWindow, DownloadingWindow, set the size of the 
    window and makes it visible in terminal   
    """
    
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.setGeometry(0, 0, 800, 600)
    download = downloading_feature.DownloadingWindow() 
    search = search_city.SearchWindow(window)  
    window.show()   

    sys.exit(app.exec())