# Copyright <2022> <Angela Erpenbeck, Ella Fink, Wiebke Janßen >

from PySide6.QtWidgets import (QWidget, QProgressBar, QLabel)
from PySide6.QtWidgets import QPushButton
from PySide6.QtCore import (QThread, Signal)
from urllib.request import urlopen 
from zipfile import ZipFile 

class DownloadingWindow(QWidget):
    """ Class to create the download window with:
            - a process bar
            - progress in percent
            - explanations 
            - start button
            - feedback whether download was successful
    
    source: https://pythonassets.com/posts/download-file-with-progress-bar-pyqt-pyside/
    """

    def __init__(self):
        """ Constructor of the DownloadingWindow class
        Initialises the title, the buttons, the labels and the progress bar
        and set the geometry of the pup up downloading window after pressing the 
        download button of the menu bar

        """

        # 
        super().__init__()
        self.setWindowTitle("Download latest map data")          
        self.resize(400, 200)
        self.label = QLabel("Press the button to start downloading.",self)
        self.label.setGeometry(20, 20, 200, 25)
        self.button = QPushButton("Start download", self)
        self.button.move(20, 60)
        self.button.pressed.connect(self.init_Download)
        self.progressBar = QProgressBar(self)
        self.progressBar.setGeometry(20, 115, 300, 25)

    def init_Download(self):
        """ starts the download, sets the label during the download,
        connects the signal of downloading progress with the process bar and
        delete the old download data  
        """

        # set label and enable button during the download 
        self.label.setText("Downloading file...")
        self.button.setEnabled(False)

        # starts the download thread
        self.downloader = Downloader(
            "https://download.gtfs.de/germany/fv_free/latest.zip",
            "latest.zip")

        # connect infomation from thread with progress bar to show the 
        # downloading progress
        self.downloader.setTotalProgress.connect(self.progressBar.setMaximum) 
        self.downloader.setCurrentProgress.connect(self.progressBar.setValue)
        
        # send infomation about the successfuly download and finished the thread 
        # afterwords
        self.downloader.succeeded.connect(self.download_succeeded)
        self.downloader.finished.connect(self.download_finished)
        self.downloader.start()     

    def download_succeeded(self):
        """ set the process bar at 100% when the download was
        successful and gives the user feedback
        """

        # Set the progress at 100%.
        self.progressBar.setValue(self.progressBar.maximum())

        # writes feedback in the label
        self.label.setText("Downloading completed!")

    def download_finished(self):
        """ reactiate the download button, deletes the 
        thread and extract the new downloaded file 'latest.zip'
        """

        # Reativate the button
        self.button.setEnabled(True)

        # Delete the thread
        del self.downloader

        # Extract the 'latest.zip'-file
        ZipFile("latest.zip").extractall("Map_Data")

class Downloader(QThread):
    """ Class to  implement the download function by 
    initialise Signals for establishing the max value of progress bar, 
    for the increasing progress and the successfully download      - 
    
    source: https://pythonassets.com/posts/download-file-with-progress-bar-pyqt-pyside/
    """
    
    # Signal for the window to establish the maximum value
    # of the progress bar
    setTotalProgress = Signal(int)

    # Signal to increase the progress
    setCurrentProgress = Signal(int)

    # Signal to be emitted when the file has been downloaded successfully
    succeeded = Signal()

    # Url of the latest zip railway data 
    url = 'https://download.gtfs.de/germany/fv_free/latest.zip'
    
    def __init__(self, url, filename):
        """ constructor of the Downloader class
        transfers url and filename of the download file

        Args: url (parameter) = url of the download file
              filename (parameter) = filename of the download file
        """

        # Instantition of url and filename
        super().__init__()
        self._url = url
        self._filename = filename
        
    def run(self):
        """ function to run the download by using the url, read 
        the file with chunksize and emit the number of bytes 
        """

        # Define url, filename, chunksize and initialise readBytes
        url = "https://download.gtfs.de/germany/fv_free/latest.zip"
        filename = "latest.zip"
        readBytes = 0
        chunkSize = 1024
        
        # Open the url of downloading data file
        with urlopen(url) as r:
            
            # Amount of bytes to be downloaded
            self.setTotalProgress.emit(int(r.info()["Content-Length"]))

            with open(filename, "ab") as f:
                while True:
    
                    # Filename is being read out in chunksize peaces
                    chunk = r.read(chunkSize)

                    # Continue if no chunk is downloaded
                    if chunk is None:
                        continue

                    # Downoad is complete when chunk is empty
                    elif chunk == b"":
                        break

                    # Writes the chunks in f
                    f.write(chunk)
                    readBytes += chunkSize
                    
                    # Transfers the number of reading bytes 
                    self.setCurrentProgress.emit(readBytes)
                
        # download is finished
        self.succeeded.emit()