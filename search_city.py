from PySide6.QtWidgets import (QLineEdit, QFormLayout, QHBoxLayout)
from PySide6.QtWidgets import (QWidget, QVBoxLayout, QPushButton, QCompleter)
from PySide6 import QtGui
import process_data

class SearchWindow(QWidget):
   """ Class to create a search window with the possibility to
       search a city of the map
   """

   def __init__(self, main_window):
      super().__init__()
      """ constructor to set the layout of the search window
      with 'Search' and 'Cancel' Button and a line to write down
      the city which should be searched
      """
      
      # set the layout of the window
      search_layout = QVBoxLayout()
      self.main_window = main_window
      self.setLayout(search_layout)

      # read in the data via pandas
      self.dataframe, self.stop_name_dict, self.data_city = \
                        process_data.data_merge()

      # add the line for searching 
      self.search = QFormLayout()
      self.city = QLineEdit(self)
      self.city.textChanged.connect(self.textchange_start)
      self.search.addRow("City", self.city)

      # add the buttons 
      self.button_layout = QHBoxLayout()
      self.button_ok = QPushButton("Search")
      self.button_ok.setDefault(True)
      self.button_ok.clicked.connect(self.search_text_city)
      self.button_cancel = QPushButton("Cancel")
      self.button_cancel.clicked.connect(self.close)
      self.button_layout.addWidget(self.button_ok)
      self.button_layout.addWidget(self.button_cancel)
      
      # add the layouts of buttons and line to the search window
      # set the geometry and the title of the window
      search_layout.addLayout(self.search)
      search_layout.addLayout(self.button_layout)
      self.setLayout(search_layout)
      self.setWindowTitle('Seach')
      self.setGeometry(400, 400, 300, 50)

   def textchange_start(self, text):
      """function to get the written string of the line box """
      
      # delivered the text to the variable text
      self.text = self.city.text()

   def search_text_city(self):
      """ function to compare the searched city and the downloaded city data,
      if searched city is in the download, a red ellipse is brushed on 
      the map """

      # read in the data via pandas
      #self.dataframe, self.stop_name_dict, self.data_city = \
      #             process_data.data_merge()
      text_search = self.text
      search_city_list = None
      found = False

      # reading the city names from the data frame of the read data
      # and pass to the variable 'search_city_list'
      for idx, row in self.dataframe.iterrows():
         if row.stop_name == text_search:
            search_city_list = row
            found = True
            break
      
      # if query to brush the ellipse on the map, if it s not found
      # a terminal print inform the user
      if found == True:
         search_result = self.main_window.map_view.scene().addEllipse\
                           (search_city_list.stop_lon, 
                            search_city_list.stop_lat, 
                            0.04,0.04,
                            pen=QtGui.QPen("red"),
                            brush=QtGui.QBrush("red"))
         return search_result

      else:
         print('nicht gefunden')         