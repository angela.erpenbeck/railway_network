# Interaktive Streckenkarte der Deutschen Bahn
 
Es wird das aktuelle Schienenfernverkehr der DB in Deutschland dargestellt.
Der Start des Programmes benötigt ungefähr 30 Sekunden.
Es besteht die Möglichkeit (Funktion):
  1)  des Zoomens zur detailierteren Dargestellung:
	- es wird das Scrollrad der Maus verwendet
	- es wird immer in der Bereich unterhalb des Mauszeigers gezoomt
	- anschließend kann mittels der rechten Maustaste und 
	  Mausbewegung der Kartenausschnitt verändert werden (DragMode)
  2)  des Downloads des aktuellen Kartenmaterials:
	- es wird das Kartenmaterial von der Website:
          		https://gtfs.de/de/feeds/de_fv/  	heruntergeladen
	- die ersetzte 'latest.zip' Datei wird in den Ordner 'Map_Data' exportiert
  3)  des Updates des Kartenmaterials:
	- die Karte wird mit den zuvor herunter geladenen Daten neu geladen
  4)  der Ausgabe der Fahrtstrecken in einer Tabelle (rechts) und 
        als  Linien in der Karte:
	- durch das Klicken mit der rechten Maustaste auf eine Stadt 
	  erscheint auf der rechtsstehenden Tabelle eine Liste mit den 
	  Bahnstrecken (Trip ID), den Ankunfts- und Abfahrtzeiten sowie 
	  der Endtstation der Strecke  von einer bestimmten Stadt
	- die Bahnstecken von der entsprechenden Stadt werden in die Karte
	  gezeichnet, um sich das Netz anzugucken wird das Scrollrad verwendet.
	  Wenn auf keine Stadt geklickt wird erscheint eine leere Tabelle mit der
	  passenden Überschrift. 
  5) die Ausgabe der ReadMe und der Lizenzen:
	- die ReadMe wird als Hilfe unter dem Reiter 'help' angezeigt
	- die Lizenz ebenfalls unter dem Reiter 'help'
  6) die Ausgabe der Bahnstrecken nach einer gewählten Suche (unvollständig):
	- das Suchfenster öffnet sich unter dem Reiter 'search' 
	- nach Eingabe einer Stadt und Bestätgung des 'Search' Buttons, 
	  wird diese auf der Karte rot markiert
	- mit dem Button 'cancel' wird das Suchfenster geschlossen
  7) die Auswahl des Streckennetzes (unvollständig):
	- die Buttons unter der Menü-Leiste zeigen die unterschiedlichen Streckennetzte
<<<<<<< HEAD
	- die Buttons können geklickt werden, jedoch ist die Auswahlfunktion nicht implementiert
=======
	- beim Anklicken dieser, wird das entsprechende Bahnnetz gewählt

>>>>>>> 3c2ada41125c70ee06c680d6b8f744bf16d24aaf
Die nötigen Vorraussetzungen für die Nutzung des Programms befinden
sich in der Textdatei: Requirements.txt





