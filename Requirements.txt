Requirements:

## Python 3.10 (Distribution)
https://www.python.org/downloads/

## Git Repositorie Clonen
https://gitlab.gwdg.de/angela.erpenbeck/railway_network.git

## Installation von Modulen
Es werden folgende Module benötigt:
	- os
	- PySide6
	- pandas
	- json
	- sys
	- urllib
	
Sie können unter Windows im Terminal mit: py -3.10 -m pip install *Modul* installiert werden.  
	
## Starten der Software
In dem selben Ordner wie das Modul 'railway_network.py' müssen zum STARTEN des Programmes, 
die Module 'downloading_feature.py', 'process_data.py' und 'search_city.py',
sowie die Text Datei 'ReadMe.txt', 'License_BDS.txt', die 'latest.zip' Datei 
von der Homeage gtfs.de/ (siehe ReadMe) und die beiden Geojson Dateien 
'bundeslaender.geojason' sowie 'countries.geojson' zum Zeichnen der Länder liegen. 

Weitere Dokumentationen befinden sich als Kommentare im Code!