# Copyright <2022> <Angela Erpenbeck, Ella Fink, Wiebke Janßen >
import pandas as pd

def timetable(str):
    """ Reads the needed data from files to create timetable containing 
    Args:
    returns:
        timetable (pandas DataFramePandas) =
    source: https://pythonassets.com/posts/download-file-with-progress-bar-pyqt-pyside/
    """
    
    #if query to determine which data needs to be read;
    #read data needed for timetable information and deleting not needed
    #columns
    if str == 'long_distance':
        data1 = pd.read_csv("Map_Data/stop_times.txt")
        data1.drop(['pickup_type','drop_off_type'],inplace=True,axis=1)
        
        data2 = pd.read_csv("Map_Data/stops.txt")
        data2.drop(['stop_lat', 'stop_lon'],inplace=True,axis=1)
    
    elif str == 'regional':
        data1 = pd.read_csv("Schienenregionalverkehr_R_S/stop_times.txt")
        data1.drop(['pickup_type','drop_off_type'],inplace=True,axis=1)
        
        data2 = pd.read_csv("Schienenregionalverkehr_R_S/stops.txt")
        data2.drop(['stop_lat', 'stop_lon'],inplace=True,axis=1)
    
    elif str == 'local':
        data1 = pd.read_csv("Oeffentlicher_Nahverkehr_Tram_Bus_U/stop_times.txt")
        data1.drop(['pickup_type','drop_off_type'],inplace=True,axis=1)
        
        data2 = pd.read_csv("Oeffentlicher_Nahverkehr_Tram_Bus_U/stops.txt")
        data2.drop(['stop_lat', 'stop_lon'],inplace=True,axis=1)
        
    else:
        data1 = pd.read_csv("Map_Data/stop_times.txt")
        data1.drop(['pickup_type','drop_off_type'],inplace=True,axis=1)
        
        data2 = pd.read_csv("Map_Data/stops.txt")
        data2.drop(['stop_lat', 'stop_lon'],inplace=True,axis=1)
        
    #merge into one dataframe
    timetable = pd.merge(pd.DataFrame(data1), 
                        pd.DataFrame(data2), 
                        left_on = ['stop_id'], 
                        right_on = ['stop_id'], 
                        how = 'left')
    
    #for-loop to find the indivudual trip_ids and their highest stop_sequence
    #and corresponding stop_name; writes them into new coloumn of the dataframe
    endstation =[]
    destinations = []
    for n in range(len(timetable)):
        trip = timetable.iloc[n]['trip_id']
        trip = timetable[timetable['trip_id'] == trip]
        rankstops = trip.sort_values(['stop_sequence'], ascending=False)
        destination = rankstops.iloc[0]
        destinations.append(destination['stop_name'])
        
        dep_time = (timetable.iloc[n])['stop_sequence']
        laststop = destination['stop_name'] 
        endstation.append(laststop)
    
    timetable['endstation'] = endstation
    
    timetable [['trip_id','endstation','arrival_time','departure_time','stop_id', 'stop_sequence','stop_name']]
    
    return timetable
