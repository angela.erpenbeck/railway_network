# Copyright <2022> <Angela Erpenbeck, Ella Fink, Wiebke Janßen >

import sys
import pandas as pd
import json

def data_merge():
    """ reads the city data out of 'stops.txt' file and 'stop_times.txt'  
        with the module pandas and merge them to one file 
        

        return: 
            dataframe (pandas.core.frame.DataFrame) 
                    = consists of the trip id, arrival and departure times, 
                    the stop id, the stop sequence, coordinates,  
                    names of the stops of long-distance trains
            trip_id_dict (dict) = has as Keys the trip ids and as values
                                  the row of the dataframe of the trip id 
            stop_name_dict (dict) = contains the stop_names as keys and 
                                    the all fitting trip id of the 
                                    cities as values  
                                       
        source: 
        https://stackoverflow.com/questions/34227038/python-pandas-merge-keyerror
        """
    # loading the data 'stops.txt' and 'stop_times'
    data_city = pd.read_csv('Map_Data\\stops.txt', 
                                usecols=['stop_lat','stop_lon',
                                        'stop_name', 'stop_id'])
    data_trip_id =  pd.read_csv('Map_Data\\stop_times.txt',
                                usecols=['trip_id','stop_id',
                                'stop_sequence','arrival_time',
                                'departure_time'])
    # merging the relevant data in dataframe 
    dataframe = pd.merge(pd.DataFrame(data_trip_id),
                        pd.DataFrame(data_city),
                        left_on = ['stop_id'], 
                        right_on = ['stop_id'], 
                        how = 'left')
    # loop to select the trip id from dataframe in a dict 
    # if query to append just unique trip ids
    # if query for stop names dict with the value of the trip id 
    # of the city (stop_name)
    trip_id_dict = {} 
    stop_name_dict = {}
    for idx, row in dataframe.iterrows():
        if row.trip_id not in trip_id_dict:
            trip_id_dict[row.trip_id] = []
        trip_id_dict[row.trip_id].append(row)
        
        if row.stop_name not in stop_name_dict:
            stop_name_dict[row.stop_name] = []
        stop_name_dict[row.stop_name].append(row.trip_id)
    
    return dataframe, trip_id_dict, stop_name_dict
    
    
def load_map_data():
    """ load the map data out of geojson files 

    source of data: 
    https://github.com/isellsoap/deutschlandGeoJSON (Germany)
    https://www.naturalearthdata.com/downloads/10m-physical-vectors/ 
    (Europe)
    
    return: data_countrys (list) = list with polygons of Germany and 
            and the countries which can be reached by long-distance train 
            from Germany
    """
   
    # reading in the geojson files of Germany and Europe
    with open('bundeslaender.geojson', 'rt') as b:
        data_b = json.load(b)
    with open('countries.geojson', 'rt') as c:
        data_c = json.load(c)

    # passing the polygons of the countries to data_countrys
    data_countrys = [
            data_c['features'][16]["geometry"]['coordinates'],
            data_c['features'][19]["geometry"]['coordinates'],
            data_c['features'][19]["geometry"]['coordinates'],
            data_c['features'][40]["geometry"]['coordinates'],
            data_c['features'][60]["geometry"]['coordinates'],
            data_c['features'][77]["geometry"]['coordinates'][11],
            data_c['features'][77]["geometry"]['coordinates'][12],
            data_c['features'][77]["geometry"]['coordinates'][13],
            data_c['features'][77]["geometry"]['coordinates'][14],
            data_c['features'][77]["geometry"]['coordinates'][15],
            data_c['features'][77]["geometry"]['coordinates'][16],
            data_c['features'][77]["geometry"]['coordinates'][20],
            data_c['features'][101]["geometry"]['coordinates'],
            data_c['features'][133]["geometry"]['coordinates'],
            data_c['features'][137]["geometry"]['coordinates'],
            data_c['features'][168]["geometry"]['coordinates'][3],
            data_c['features'][168]["geometry"]['coordinates'][4],
            data_c['features'][168]["geometry"]['coordinates'][5],
            data_c['features'][168]["geometry"]['coordinates'][6],
            data_c['features'][168]["geometry"]['coordinates'][7],
            data_c['features'][168]["geometry"]['coordinates'][8],
            data_c['features'][168]["geometry"]['coordinates'][9],
            data_c['features'][168]["geometry"]['coordinates'][10],
            data_c['features'][168]["geometry"]['coordinates'][11],
            data_c['features'][182]["geometry"]['coordinates'],
            data_c['features'][213]["geometry"]['coordinates'],
            data_c['features'][214]["geometry"]['coordinates']
        ]        
    
    # loop to read German states with multipolygons
    x = 0
    while x < 16:
        data_b_type = data_b['features'][x]["geometry"]['type']
        data_b_coordinates = data_b['features'][x]["geometry"]['coordinates']
        x += 1
        if data_b_type == 'MultiPolygon':
            for n in range(len(data_b_coordinates)):
                data_countrys.append(data_b_coordinates[n])
        else:
            data_countrys.append(data_b_coordinates)

    # loop to read countries with multipolygons
    data_c_coordinates = data_c['features'][99]["geometry"]['coordinates']
    for m in range(len(data_c_coordinates)):
        data_countrys.append(data_c_coordinates[m])
    
    data_c_coordinates = data_c['features'][64]["geometry"]['coordinates']
    for m in range(len(data_c_coordinates)):
        data_countrys.append(data_c_coordinates[m])
    
    data_c_coordinates = data_c['features'][112]["geometry"]['coordinates']
    for m in range(len(data_c_coordinates)):
        data_countrys.append(data_c_coordinates[m])
                
    return data_countrys